@extends('layouts.layout')
@section('content')
    <div class="row">
        <section class="content">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-left"><h1 class="text-primary">Libros existentes</h1></div>
                        <div class="pull-right">
                            <div class="btn-group">
                                <a href="{{ route('libro.create') }}" class="btn btn-primary">Nuevo libro</a>
                            </div>
                        </div>
                        <div class="table-resposive">
                            <table id="mytable" class="table table-bordred table-striped">
                                <thead>
                                <th><h4 class="text-info">Nombre</h4></th>
                                <th><h4 class="text-info text-justify">Resumen</h4></th>
                                <th><h4 class="text-info">No. Páginas</h4></th>
                                <th><h4 class="text-info">Edicion</h4></th>
                                <th><h4 class="text-info">Autor</h4></th>
                                <th><h4 class="text-info">Precio</h4></th>
                                <th><h4 class="text-info">Editar</h4></th>
                                <th><h4 class="text-info">Eliminar</h4></th>
                                </thead>
                                <tbody>
                                @if($libros->count())
                                    @foreach($libros as $libro)
                                        <tr>
                                            <td class="text-info">{{$libro->nombre}}</td>
                                            <td class="text-info text-justify">{{$libro->resumen}}</td>
                                            <td class="text-info text-center">{{$libro->npagina}}</td>
                                            <td class="text-info text-center">{{$libro->edicion}}</td>
                                            <td class="text-info">{{$libro->autor}}</td>
                                            <td class="text-info text-center">${{$libro->precio}}</td>
                                            <td><a class="btn btn-primary btn-xs margen-boton"
                                                   href="{{action('LibroController@edit', $libro->id)}}"><span
                                                            class="glyphicon glyphicon-pencil"></span></a></td>
                                            <td>
                                                <form action="{{action('LibroController@destroy', $libro->id)}}"
                                                      method="post">
                                                    {{csrf_field()}}
                                                    <input name="_method" type="hidden" value="DELETE">

                                                    <button class="btn btn-danger btn-xs margen-boton2" type="submit"><span
                                                                class="glyphicon glyphicon-trash"></span></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" class="text-center"><h3 class="text-info">Sin libros en la base de datos</h3></td>
                                    </tr>
                                @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                    {{ $libros->links() }}
                </div>
            </div>
        </section>
    </div>
@endsection

