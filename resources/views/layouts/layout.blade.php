<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, user-scalable=yes">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/estilos.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Eater|Muli" rel="stylesheet">
</head>
<body background="{{asset('img/fondo.jpg')}} " >

<div class=" marg-arriba">
    <h1 class="text-center text-uppercase">Bienvenido</h1>
</div>
<div class="container-fluid">
    @yield('content')
</div>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>
</html>