@extends('layouts.reportLayout')
@section('content')

    <div class="panel panel-default panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title">Librería random</h2>
        </div>
        <div class="panel-body">
            Reporte de libros en la base de datos
        </div>
    </div>

    <table class="table table-responsive table-striped ">
        <thead>
        <tr>
        <th><h4 class="text-info text-center">Nombre</h4></th>
        <th><h4 class="text-info text-justify">Resumen</h4></th>
        <th><h4 class="text-info text-center">No. Páginas</h4></th>
        <th><h4 class="text-info text-center">Edicion</h4></th>
        <th><h4 class="text-info text-center">Autor</h4></th>
        <th><h4 class="text-info text-center">Precio</h4></th>
        </tr>
        </thead>
        @foreach($libros as $libro)
        <tbody>
        <tr>
            <td>{{$libro->nombre}}</td>
            <td class="text-justify">{{$libro->resumen}}</td>
            <td class="text-center">{{$libro->npagina}}</td>
            <td class="text-center">{{$libro->edicion}}</td>
            <td>{{$libro->autor}}</td>
            <td class="text-center">{{$libro->precio}}</td>
        </tr>
        </tbody>
        @endforeach
    </table>
    <footer class="footer">
        <div class="container">
            <span class="text-muted">Instituto tecnológico del istmo</span>
        </div>
    </footer>
@endsection

