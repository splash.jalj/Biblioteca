<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libro;

class LibroController extends Controller
{

    public function index()//Consulta
    {
        //
        $libros = Libro::orderBy('id', 'DESC')->paginate(5);//mandar a llamar registros y luego ordenar
        return view('Libro.index', compact('libros')); //compact manda la informacion de los libros
    }
    public function create()
    {
        //
        return view('Libro.create');
    }
    public function store(Request $request)//ALTAS
    {
        //
        $this->validate($request, ['nombre' => 'required|max:30', 'resumen' => 'required|max:375',  'edicion' => 'required|numeric', 'autor' => 'required|max:20', 'npagina' => 'required', 'precio' => 'required']);
        Libro::create($request->all());
        return redirect()->route('libro.index');
    }
    public function edit($id)
    {
        //
        $libro = libro::find($id);
        return view('libro.edit', compact('libro'));
    }

    public function update(Request $request, $id)//MODIFICAR
    {
        //
        $this->validate($request, ['nombre' => 'required|max:30', 'resumen' => 'required|max:375', 'edicion' => 'required|numeric', 'autor' => 'required|max:20', 'npagina' => 'required', 'precio' => 'required']);
        libro::find($id)->update($request->all());//Aqui se actualiza y se guarda en la base de datos
        return redirect()->route('libro.index');

    }
    public function destroy($id)//BAJAS
    {
        //
        Libro::find($id)->delete();//Se busca por su id
        return redirect()->route('libro.index');
    }


}
