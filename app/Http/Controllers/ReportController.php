<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libro;
use Barryvdh\DomPDF\Facade as PDF;

class ReportController extends Controller
{
    //
    /**
     * @return mixed
     */
    public function generar()
    {
        //$libros=Libro::all();
        $libros =\DB::table('libros')->select(['nombre', 'resumen', 'npagina','edicion','autor','precio'])->get();
        //dd($libros);
        $pdf = PDF::loadView('reporte', compact('libros'));
        //dd($pdf);
        return $pdf->stream();
    }
}
